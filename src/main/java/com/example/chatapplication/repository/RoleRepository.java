package com.example.chatapplication.repository;


import com.example.chatapplication.entity.Role;
import com.example.chatapplication.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRoleName(RoleName roleName);
}
