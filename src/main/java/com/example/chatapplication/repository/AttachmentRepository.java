package com.example.chatapplication.repository;

import com.example.chatapplication.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;


import java.util.List;
import java.util.UUID;

@CrossOrigin
public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
    void deleteAllById(UUID id);

//    List<Attachment > findAttachmentsBy(UUID uuid);
}
