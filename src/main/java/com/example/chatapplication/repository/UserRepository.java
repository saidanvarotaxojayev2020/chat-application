package com.example.chatapplication.repository;


import com.example.chatapplication.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    User findByPhoneNumber(String phoneNumber);

    boolean existsByPhoneNumber(String phoneNumber);
//
////    Optional<User> findByPhoneNumber(String phoneNumber);
//
//    @Modifying
//    @Query(value = "select users.* from users join users_roles ur on users.id = ur.users_id join role r on ur.roles_id = r.id and r.role_name = :ketmon limit :size offset (:page * :size)", nativeQuery = true)
//    List<User> byUserRole(@Param(value = "ketmon") String roleName, @Param(value = "page") Integer page, @Param(value = "size") Integer size);
//
//    List<User> findAllByPhoneNumberContainingIgnoreCaseOrFirstNameContainingIgnoreCase(String phoneNumber, String firstName);
}
