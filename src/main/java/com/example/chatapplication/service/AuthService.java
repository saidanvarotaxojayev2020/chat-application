package com.example.chatapplication.service;

import com.example.chatapplication.controller.AuthController;
import com.example.chatapplication.entity.Attachment;
import com.example.chatapplication.entity.User;
import com.example.chatapplication.payload.ApiResponse;
import com.example.chatapplication.payload.ReqSignIn;
import com.example.chatapplication.payload.UserDto;
import com.example.chatapplication.repository.AttachmentRepository;
import com.example.chatapplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
    Created by Saidanvar
    03.06.2021
*/

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AuthController authController;


    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(phoneNumber);
    }

    public UserDetails loadUserById(UUID userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User id not found: " + userId));
    }

    public ApiResponse registerUser(UserDto userDto) {
        try {
            User byPhoneNumber = userRepository.findByPhoneNumber(userDto.getPhoneNumber());

            if (byPhoneNumber != null)
                return new ApiResponse("This phone number is already registered", false);

            if (!userDto.getPassword().equals(userDto.getPrePassword()))
                return new ApiResponse("Parollar mos emas", false);

            ApiResponse apiResponse = makePhoneNumberPattern(userDto.getPhoneNumber());
            if (apiResponse.isSuccess()) {
                User user = new User();
                user.setFirstName(userDto.getFirstName());
                user.setLastName(userDto.getLastName());
                user.setPhoneNumber(userDto.getPhoneNumber());
                user.setPassword(passwordEncoder.encode(userDto.getPassword()));
                if (userDto.getAttachmentIds() != null ) {
                    Set<Attachment> attachmentSet = new HashSet<>();

                    for (UUID attachmentId : userDto.getAttachmentIds()) {
                        Attachment attachment_not_found = attachmentRepository.findById(attachmentId)
                                .orElseThrow(() -> new IllegalStateException("Attachment not found"));
                        attachmentSet.add(attachment_not_found);
                    }
                    user.setAttachments(attachmentSet);
                }
                user.setEnabled(true);
                userRepository.save(user);

                ReqSignIn reqSignIn = new ReqSignIn();
                reqSignIn.setPhoneNumber(userDto.getPhoneNumber());
                reqSignIn.setPassword(userDto.getPassword());
                String jwt = authController.getJwt(reqSignIn);
                return new ApiResponse("Success", true, jwt);
            } else {
                return apiResponse;
            }
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse makePhoneNumberPattern(String phoneNumber) {
        if (phoneNumber != null) {
            phoneNumber = phoneNumber.startsWith("+") ? phoneNumber : "+" + phoneNumber;
            String regex = "^[+][9][9][8][0-9]{9}$";

            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(phoneNumber);

            if (matcher.matches()) {
                return new ApiResponse("", true);
            } else {
                return new ApiResponse("Raqam formati xato, Iltimos tekshiring", false);
            }
        } else {
            return new ApiResponse("Iltimos telefon raqamingizni kiriting", false);
        }
    }
}
