package com.example.chatapplication.service;

import com.example.chatapplication.entity.Attachment;
import com.example.chatapplication.entity.AttachmentContent;
import com.example.chatapplication.payload.ApiResponse;
import com.example.chatapplication.repository.AttachmentContentRepository;
import com.example.chatapplication.repository.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;


import java.io.IOException;
import java.util.UUID;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public ApiResponse deleteAttachment(UUID id) {
        try {
            attachmentContentRepository.deleteById(id);
            return new ApiResponse("Successfully Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }


    public UUID uploadFile(MultipartHttpServletRequest request) {
        try {
            MultipartFile file = request.getFile("file");
            assert file != null;
            Attachment attachment = attachmentRepository.save(new Attachment(
                    file.getOriginalFilename(),
                    file.getContentType(),
                    file.getSize()
            ));

            try {
                attachmentContentRepository.save(new AttachmentContent(
                        attachment,
                        file.getBytes()
                ));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return attachment.getId();
        } catch (Exception e) {
            return null;
        }
    }

    public HttpEntity<?> getFile(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("attachment"));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(id).orElseThrow(() -> new ResourceNotFoundException("attachmentContent"));
        return ResponseEntity
                .ok()
                .contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getBytes());
    }
}
