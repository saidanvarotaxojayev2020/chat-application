package com.example.chatapplication.service;

/*
 Created azamat_azadov 03/06/2021
 */

import com.example.chatapplication.entity.Attachment;
import com.example.chatapplication.entity.User;
import com.example.chatapplication.payload.ApiResponse;
import com.example.chatapplication.payload.UserDto;
import com.example.chatapplication.repository.AttachmentContentRepository;
import com.example.chatapplication.repository.AttachmentRepository;
import com.example.chatapplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthService authService;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public ApiResponse editUser(UserDto userDto) {
        try {
            User userById = userRepository.findById(userDto.getId()).orElseThrow(() -> new IllegalStateException("User not found"));
            if (!userById.getPhoneNumber().equals(userDto.getPhoneNumber())) {
                if (userRepository.existsByPhoneNumber(userDto.getPhoneNumber())) {
                    return new ApiResponse("Already phone", false);
                }
            }
            ApiResponse apiResponse = authService.makePhoneNumberPattern(userDto.getPhoneNumber());
            if (apiResponse.isSuccess()) {
                userById.setPhoneNumber(userDto.getPhoneNumber());
            } else {
                return apiResponse;
            }
            userById.setPassword(userDto.getPassword());
            userById.setFirstName(userDto.getFirstName());
            userById.setLastName(userDto.getLastName());
            if (userDto.getAttachmentIds() != null) {
                Set<Attachment> attachmentSet = new HashSet<>();

                for (UUID attachmentId : userDto.getAttachmentIds()) {
                    Attachment attachment_not_found = attachmentRepository.findById(attachmentId)
                            .orElseThrow(() -> new IllegalStateException("Attachment not found"));
                    attachmentSet.add(attachment_not_found);
                }
                userById.setAttachments(attachmentSet);
            }

            userRepository.save(userById);

            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse deleteUser(UUID userID) {
        try {
            userRepository.deleteById(userID);
            return new ApiResponse("Accountingiz o`chirildi", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}
