package com.example.chatapplication.controller;

import com.example.chatapplication.payload.ApiResponse;
import com.example.chatapplication.payload.ReqSignIn;
import com.example.chatapplication.payload.UserDto;
import com.example.chatapplication.security.JwtTokenProvider;
import com.example.chatapplication.service.AuthService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
    Created by Saidanvar
    03.06.2021 
*/
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthService authService;

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqSignIn reqSignIn) {
        return ResponseEntity.ok(getJwt(reqSignIn));
    }

    public String getJwt(@NotNull ReqSignIn reqSignIn) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(reqSignIn.getPhoneNumber(), reqSignIn.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtTokenProvider.generateToken(authentication);
    }

    @PostMapping("/registerUser")
    private HttpEntity<?> registerUser(@RequestBody UserDto userDto){
        ApiResponse apiResponse = authService.registerUser(userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }
}
