package com.example.chatapplication.controller;

/*
 Created azamat_azadov 03/06/2021
 */

import com.example.chatapplication.entity.User;
import com.example.chatapplication.payload.ApiResponse;
import com.example.chatapplication.payload.UserDto;
import com.example.chatapplication.repository.UserRepository;
import com.example.chatapplication.security.CurrentUser;
import com.example.chatapplication.service.UserService;
import com.sun.deploy.association.utility.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @PutMapping("/editUser")
    public HttpEntity<?> editProfile(@RequestBody UserDto userDto) {
        ApiResponse response = userService.editUser(userDto);
        return ResponseEntity.status(response.isSuccess() ? 202 : 409).body(response);
    }

    @DeleteMapping("/deleteUser/{userID}")
    public HttpEntity<?> deleteUser(@PathVariable UUID userID) {
        ApiResponse apiResponse = userService.deleteUser(userID);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}

