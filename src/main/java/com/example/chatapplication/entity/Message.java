package com.example.chatapplication.entity;

import com.example.chatapplication.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Message extends AbsEntity {


    private String body;

    @OneToOne
    private User sender;

    @OneToOne
    private User receiver;

    @OneToMany
    private Set<Attachment> attachments;
}
