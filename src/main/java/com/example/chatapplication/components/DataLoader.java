package com.example.chatapplication.components;

import com.example.chatapplication.entity.Role;
import com.example.chatapplication.entity.User;
import com.example.chatapplication.entity.enums.RoleName;
import com.example.chatapplication.repository.RoleRepository;
import com.example.chatapplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {

//            Date date = new Date(1998-01-20);
            Role user = roleRepository.save(new Role(RoleName.ROLE_USER));
            userRepository.save(
                    new User(
                            "Saidanvar",
                            "Otaxojayev",
                            "+998995343369",
                            "123",
                            Collections.singletonList(user),
                            true
                    )
            );
        }
    }
}
