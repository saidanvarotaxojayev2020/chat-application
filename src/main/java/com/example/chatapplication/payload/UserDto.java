package com.example.chatapplication.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private UUID id;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String username;

    private String password;

    private String prePassword;

    private List<UUID> attachmentIds;

    private List<UserDto> friendsDto;
}
