package com.example.chatapplication.payload;

import lombok.Data;

@Data
public class ReqSignIn {
    private String phoneNumber;
    private String password;
}
