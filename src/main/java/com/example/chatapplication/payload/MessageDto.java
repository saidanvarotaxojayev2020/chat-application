package com.example.chatapplication.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

/*
    Created by Saidanvar
    02.06.2021 
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {

    private UUID id;

    private String body;

    private UserDto senderDto;

    private UserDto receiverDto;

    private Set<UUID> attachmentIds;
}
